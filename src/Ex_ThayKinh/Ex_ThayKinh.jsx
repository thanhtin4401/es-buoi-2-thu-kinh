import React, { Component } from "react";
import { glassData } from "./data";
// import ItemGlass from "./ItemGlass";

export default class Ex_ThayKinh extends Component {
  state = {
    glassData: glassData,
    glassChoice: ()=>{

    },
    inforGlass: () => {
      
    },
  };

  renderGlass = () => {
    return this.state.glassData.map((item) => {
      return (
        <div
          className="col-4"
          id={item.id}
          onClick={() => {
            this.handleChoiceGlass(
              item.id,
              item.price,
              item.name,
              item.url,
              item.desc
            );
          }}
        >
          <img src={item.url} alt="" style={{ width: "100%" }} />
        </div>
      );
    });
  };
  handleChoiceGlass = (id, price, name, url, desc) => {
 this.setState({
   glassChoice:()=>{
     return(
       <img src={url} alt="" />
     )
   },
   inforGlass:()=>{
    return (
      <div>
  <h1 className="header">{name}</h1>
  <div className="price_container d-flex align-items-center my-2">
    <h3 className="price mr-2 bg-danger rounded">{price}</h3>
    <h3 className="stocking">stocking</h3>
  </div>
  <p className="title">{desc}</p>
</div>

    );
   }
 })
  };
  render() {
    
    return (
      
      <div className="container vglasses py-3">
        <div className="row  justify-content-between">
          <div className="col-6 vglasses__left">
            <div className="row">
              <div className="col-12">
                <h1 className="mb-2">Virtual Glasses</h1>
              </div>
            </div>
            <div
              className="row "
              id="vglassesList"
              style={{ width: "100%", height: "100%" }}
            >
              {this.renderGlass()}
            </div>
          </div>
          <div className="col-5 vglasses__right p-0">
            <div className="vglasses__card">
              <div className="mb-2 text-right mt-2 mr-2">
                <button
                  className="btn btn-warning"
         
                >
                  Before
                </button>
                <button
                  className="btn btn-warning"
                 
                >
                  After
                </button>
              </div>
              <div className="vglasses__model" id="avatar">
               {this.state.glassChoice()}
               
              </div>
              <div id="glassesInfo" className="vglasses__info">
                {this.state.inforGlass()}
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}
